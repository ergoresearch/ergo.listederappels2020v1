<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="LR201011.aspx.cs" Inherits="Page_LR201011" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="Ergo.ListeRappels2018V1.Graph.EPRappelMaint09"
        PrimaryView="AvailableTasks">
		<CallbackCommands>
		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView Height="100%" ID="form" runat="server" DataSourceID="ds" DataMember="AvailableTasks" Width="100%" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXDateTimeEdit Width="80px" CommitChanges="True" runat="server" ID="CstPXDateTimeEdit2" DataField="ForDate" ></px:PXDateTimeEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule8" StartColumn="True" ></px:PXLayoutRule>
			<px:PXDropDown Width="" CommitChanges="True" runat="server" ID="CstPXDropDown4" DataField="TaskType" >
				<AutoCallBack>
					<Behavior RepaintControls="None" ></Behavior></AutoCallBack></px:PXDropDown>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule32" StartColumn="True" />
			<px:PXSelector Width="120px" CommitChanges="True" runat="server" ID="CstPXSelector1" DataField="BranchID" ></px:PXSelector>
			<px:PXDropDown Width="120px" CommitChanges="True" runat="server" ID="CstPXDropDown3" DataField="Region" ></px:PXDropDown>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule33" StartColumn="True" />
			<px:PXNumberEdit Enabled="False" runat="server" ID="CstPXNumberEdit10" DataField="UsrRappelsTotal" ></px:PXNumberEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule34" StartColumn="True" />
			<px:PXNumberEdit Enabled="False" runat="server" ID="CstPXNumberEdit9" DataField="UsrFiltresRappelsTotal" ></px:PXNumberEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule15" StartRow="True" ></px:PXLayoutRule>
			<px:PXGrid AutoAdjustColumns="True" RepaintColumns="False" AdjustPageSize="Auto" Height="250px" Width="100%" Caption="Rappels dsponibles" CaptionVisible="True" SkinID="Details" AllowPaging="True" SyncPosition="True" ValidateRequestMode="Enabled" PageSize="15" FilesIndicator="False" NoteIndicator="False" DataSourceID="ds" runat="server" ID="CstPXGrid16">
				<Levels>
					<px:PXGridLevel DataMember="GridFilterAvailableTasks" >
						<Columns>
							<px:PXGridColumn DataField="UIStatus" Width="70" ></px:PXGridColumn>
							<px:PXGridColumn DataField="UsrBranchCD" Width="140" ></px:PXGridColumn>
							<px:PXGridColumn DataField="EndDate" Width="90" ></px:PXGridColumn>
							<px:PXGridColumn LinkCommand="viewActivity" DataField="Subject" Width="280" ></px:PXGridColumn>
							<px:PXGridColumn LinkCommand="viewPatient" DataField="Source" Width="70" ></px:PXGridColumn>
							<px:PXGridColumn DataField="Phone1" Width="180" ></px:PXGridColumn>
							<px:PXGridColumn DataField="Phone2" Width="180" ></px:PXGridColumn>
							<px:PXGridColumn DataField="Phone3" Width="180" ></px:PXGridColumn>
							<px:PXGridColumn DataField="OwnerID" Width="70" ></px:PXGridColumn>
							<px:PXGridColumn DataField="Body" Width="70" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
				
				<AutoCallBack Command="Refresh" Target="grdCustomerActivitiesDetails100" ActiveBehavior="True">
					<Behavior RepaintControlsIDs="grdCustomerActivitiesDetails100" CommitChanges="False" RepaintControls="None" ></Behavior></AutoCallBack>
				<ActionBar ActionsText="False">
					<CustomItems>
						<px:PXToolBarButton Text="AJOUTER COMMANDE" >
							<AutoCallBack Target="ds" Command="AjouterCommande" ></AutoCallBack></px:PXToolBarButton>
						<px:PXToolBarButton Text="AJOUTER RENDEZ-VOUS" >
							<AutoCallBack Target="ds" Command="AddAppointment" ></AutoCallBack>
							<AutoCallBack Target="ds" ></AutoCallBack></px:PXToolBarButton></CustomItems>
					<Actions>
						<AddNew Enabled="False" ></AddNew>
						<Delete Enabled="False" ></Delete></Actions></ActionBar></px:PXGrid>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule26" StartRow="True" ></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule27" StartColumn="True" ></px:PXLayoutRule>
			<px:PXGrid NoteIndicator="False" FilesIndicator="False" Width="600px" PageSize="15" DataSourceID="ds" AdjustPageSize="Auto" SyncPosition="True" AutoAdjustColumns="True" AllowPaging="True" Caption="Activités Patient" CaptionVisible="True" Height="300px" runat="server" ID="grdCustomerActivitiesDetails100">
				<Levels>
					<px:PXGridLevel DataMember="CustomerActivitiesDetails" >
						<Columns>
							<px:PXGridColumn DataField="IsCompleteIcon" Width="70" ></px:PXGridColumn>
							<px:PXGridColumn DataField="StartDate" Width="90" ></px:PXGridColumn>
							<px:PXGridColumn DataField="EndDate" Width="90" ></px:PXGridColumn>
							<px:PXGridColumn LinkCommand="viewCustActivity" DataField="Subject" Width="280" ></px:PXGridColumn>
							<px:PXGridColumn DataField="Body" Width="70" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
				<AutoCallBack Target="grdCustomerOrderLinesDetails100" Command="Refresh">
					<Behavior RepaintControlsIDs="grdCustomerOrderLinesDetails100" RepaintControls="None" CommitChanges="False" ></Behavior></AutoCallBack>
				<AutoCallBack>
					<Behavior RepaintControlsIDs="grdCustomerOrderLinesDetails100" ></Behavior></AutoCallBack>
				<AutoCallBack Target="grdCustomerOrderLinesDetails100" ></AutoCallBack>
				<AutoCallBack Command="Refresh" ></AutoCallBack></px:PXGrid>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule29" StartColumn="True" ></px:PXLayoutRule>
			<px:PXGrid FilesIndicator="False" NoteIndicator="False" AdjustPageSize="Auto" PageSize="15" DataSourceID="ds" Width="600px" Caption="Historique des ventes" CaptionVisible="True" AutoAdjustColumns="True" AllowPaging="True" Height="300px" runat="server" ID="grdCustomerOrderLinesDetails100">
				<Levels>
					<px:PXGridLevel DataMember="CustomerOrderLinesDetails" >
						<Columns>
							<px:PXGridColumn LinkCommand="viewOrder" DataField="OrderNbr" Width="140" ></px:PXGridColumn>
							<px:PXGridColumn DataField="TranDate" Width="90" ></px:PXGridColumn>
							<px:PXGridColumn DataField="OrderType" Width="70" ></px:PXGridColumn>
							<px:PXGridColumn DataField="TranDesc" Width="280" ></px:PXGridColumn>
							<px:PXGridColumn DataField="TranAmt" Width="100" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels></px:PXGrid></Template>
    <AutoSize Enabled="" Container="Window" MinHeight="300" ></AutoSize>
	</px:PXFormView>
</asp:Content>
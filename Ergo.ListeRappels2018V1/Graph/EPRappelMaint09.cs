﻿using Ergo.ListeRappels2018V1.DAC.Views;
using PX.Data;
using PX.Data.BQL.Fluent;
using PX.Objects.AR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Objects.SO;
using PX.Objects.CR;
using Ergo.ListeRappels2018V1.Constants;
using System.Globalization;
using Ergo.ListeRappels2018V1.Extensions;
using PX.Objects.EP;
using PX.Objects.GL;

namespace Ergo.ListeRappels2018V1.Graph
{
    [TableAndChartDashboardType]
    public class EPRappelMaint09 : PXGraph<EPRappelMaint09>
    {
        public SelectFrom<AvailableTasks>.View AvailableTasks;
        public PXSave<AvailableTasks> Save;
        public PXCancel<AvailableTasks> Cancel;
        public PXAction<Customer> AjouterCommande;
        public PXAction<Customer> AddAppointment;
        public PXAction<CRActivity> ViewActivity;
        public PXAction<Customer> ViewPatient;
        public PXAction<CRActivity> ViewCustActivity;
        public PXAction<SOOrder> ViewOrder;

        public PXSelect<
            GridAvailableTasks,
            Where<GridAvailableTasks.endDate, Less<Current<AvailableTasks.forDate>>,
            And2<
                Where<GridAvailableTasks.usrSubType, Equal<Current<AvailableTasks.taskType>>,
                Or<Current<AvailableTasks.taskType>, IsNull,
                Or<GridAvailableTasks.createdByScreenID, Equal<ListeRappelsConstants.taskWeb>>>>,
                And2<
                    Where<GridAvailableTasks.usrBranchCD, Equal<Current<AvailableTasks.branchID>>,
                    Or<Current<AvailableTasks.branchID>, IsNull,
                    Or<GridAvailableTasks.createdByScreenID, Equal<ListeRappelsConstants.taskWeb>>>>,
                   And<
                       Where<Substring<GridAvailableTasks.subcd, ListeRappelsConstants.SubCDStartingIndex, ListeRappelsConstants.SubCDStartingLength>, Equal<Current<AvailableTasks.region>>,
                       Or<Current<AvailableTasks.region>, IsNull,
                       Or<GridAvailableTasks.createdByScreenID, Equal<ListeRappelsConstants.taskWeb>>>>>>>>,
            OrderBy<Desc<GridAvailableTasks.endDate>>> GridFilterAvailableTasks;

        public PXSelect<CustomerActivities,
                        Where<CustomerActivities.refnoteid, Equal<Current<GridAvailableTasks.refnoteid>>>,
                        OrderBy<Desc<CustomerActivities.endDate>>> CustomerActivitiesDetails;

        public PXSelect<CustomerOrderLines,
                         Where<CustomerOrderLines.noteid, Equal<Current<CustomerActivities.refnoteid>>,
                         And<CustomerOrderLines.status, Equal<SOOrderStatus.completed>>>,
                         OrderBy<Desc<ARTran.tranDate>>> CustomerOrderLinesDetails;

        public EPRappelMaint09()
        {

        }

        [PXButton]
        [PXUIField(DisplayName = "AJOUTER COMMANDE")]
        protected void ajouterCommande()
        {
            Customer customer = (Customer)PXSelect<Customer, Where<Customer.noteID, Equal<Current<GridAvailableTasks.refnoteid>>>>.Select(this);
            SOOrderEntry instance = PXGraph.CreateInstance<SOOrderEntry>();
            SOOrder poOrder = (SOOrder)instance.Document.Cache.CreateInstance();
            poOrder = instance.Document.Insert(poOrder);
            instance.Document.Cache.SetValueExt<SOOrder.customerID>(poOrder, customer.BAccountID);
            PXRedirectHelper.TryRedirect((PXGraph)instance, PXRedirectHelper.WindowMode.New);
        }

        [PXButton]
        [PXUIField(DisplayName = "AJOUTER RENDEZ-VOUS")]
        protected void addAppointment()
        {
            Customer customer = (Customer)PXSelect<Customer, Where<Customer.noteID, Equal<Current<GridAvailableTasks.refnoteid>>>>.Select(this);
            var addressQuery = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.Select(this, new object[1] { customer.DefAddressID });
            Address address = addressQuery.FirstOrDefault().GetItem<Address>();

            String postalCode = address.PostalCode;
            String patientID = customer.NoteID.ToString();
            String patientName = customer.AcctName;
            String mainURL = @"/ergoappointment/Scheduler/ergo-scheduler/scheduler.html?type=empMeetings";
            String qs = customer != null ? string.Format("&search=true&postalCode={0}&patientID={1}&patientName={2}", postalCode, patientID, RemoveDiacritics(patientName)) :
                                         string.Format("&search=true&taskID={0}", this.GridFilterAvailableTasks.Current.Noteid);

            String url = mainURL + qs;
            throw new PXRedirectToUrlException(url, PXBaseRedirectException.WindowMode.New, true, "Rendez-vous");
        }

        [PXButton]
        [PXUIField(DisplayName = "Voir Activité")]
        protected void viewActivity()
        {

            var currentGATask = this.GridFilterAvailableTasks.Current;
            var query = PXSelect<CRActivity, Where<CRActivity.noteID, Equal<Required<CRActivity.noteID>>>>.Select(this, new object[1] { currentGATask.Noteid });
            CRActivity currentActivity = query.FirstOrDefault()?.GetItem<CRActivity>() ?? null;
            String url = @"/ergoappointment/Scheduler/ergo-scheduler/scheduler.html?type=empMeetings&owner=" + (currentActivity?.OwnerID) + "&date=" + (currentActivity?.StartDate?.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds);

            if (currentActivity.Type.Trim() != "E")
            {
                CRTaskMaint maint = CRTaskMaint.CreateInstance<CRTaskMaint>();
                maint.Tasks.Current = currentActivity;
                throw new PXPopupRedirectException(maint, "Activité", true, true);
            }
            else
            {
                throw new PXRedirectToUrlException(url, PXBaseRedirectException.WindowMode.New, true, "Rendez-vous");
            }
        }

        [PXButton]
        [PXUIField(DisplayName = "Voir Patient")]
        protected void viewPatient()
        {
            Customer customer = (Customer)PXSelect<Customer, Where<Customer.noteID, Equal<Current<GridAvailableTasks.refnoteid>>>>.Select(this);
            var graph = PXGraph.CreateInstance<PX.Objects.AR.CustomerMaint>();
            graph.BAccount.Current = graph.BAccount.Search<PX.Objects.AR.Customer.acctCD>(customer.AcctCD);
            throw new PXRedirectRequiredException(graph, "View Customer");
        }

        [PXButton]
        [PXUIField(DisplayName = "Voir Patient Activité")]
        protected void viewCustActivity()
        {
            var currentCA = this.CustomerActivitiesDetails.Current;
            var query = PXSelect<CRActivity, Where<CRActivity.noteID, Equal<Required<CRActivity.noteID>>>>.Select(this, new object[1] { currentCA.Noteid });
            CRActivity currentActivity = query.FirstOrDefault()?.GetItem<CRActivity>() ?? null;
            String url = @"/ergoappointment/Scheduler/ergo-scheduler/scheduler.html?type=empMeetings&owner=" + (currentActivity?.OwnerID) + "&date=" + (currentActivity?.StartDate?.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds);

            if (currentActivity.Type.Trim() != "E")
            {
                switch (currentActivity.ClassID)
                {
                    case 0:
                        CRTaskMaint taskMaint = CRTaskMaint.CreateInstance<CRTaskMaint>();
                        taskMaint.Tasks.Current = currentActivity;
                        throw new PXPopupRedirectException(taskMaint, "Activité", true, true);
                    default:
                        CRActivityMaint actMaint = CRActivityMaint.CreateInstance<CRActivityMaint>();
                        actMaint.Activities.Current = currentActivity;
                        throw new PXPopupRedirectException(actMaint, "Activité", true, true);
                }
            }
            else
            {
                throw new PXRedirectToUrlException(url, PXBaseRedirectException.WindowMode.New, true, "Rendez-vous");
            }
        }

        [PXButton]
        [PXUIField(DisplayName = "Voir Commande")]
        protected void viewOrder()
        {
            var order = this.CustomerOrderLinesDetails.Current;
            var query = PXSelect<SOOrder, Where<SOOrder.orderNbr, Equal<Required<SOOrder.orderNbr>>>>.Select(this, new object[1] { order.OrderNbr });
            SOOrder soorder = query.FirstOrDefault()?.GetItem<SOOrder>() ?? null;

            if (soorder.OrderType == "IN")
            {
                PX.Objects.SO.SOOrderEntry instance = PXGraph.CreateInstance<PX.Objects.SO.SOOrderEntry>();
                instance.Document.Current = (soorder);
                PXRedirectHelper.TryRedirect(instance, PXRedirectHelper.WindowMode.NewWindow);
            }
            else
            {
                PX.Objects.SO.SOOrderEntry instance = PXGraph.CreateInstance<PX.Objects.SO.SOOrderEntry>();
                instance.Document.Current = (soorder);
                PXRedirectHelper.TryRedirect(instance, PXRedirectHelper.WindowMode.NewWindow);
            }
        }

        protected void AvailableTasks_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            int? nbTotal = 0, nbTotalCurrent = 0;
            var row = (AvailableTasks)e.Row;
            var rowExt = PXCache<AvailableTasks>.GetExtension<AvailableTasksExt>(row);
            var resultSet = new PXSelect<GridAvailableTasks>(this);
            nbTotal = resultSet.Select().Count;
            rowExt.UsrRappelsTotal = nbTotal;
            nbTotalCurrent = this.GridFilterAvailableTasks.Select().Count;
            rowExt.UsrFiltresRappelsTotal = nbTotalCurrent;

        }

        protected void AvailableTasks_TaskType_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (AvailableTasks)e.Row;
        }

        public string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}

﻿using Ergo.ListeRappels2018V1.DAC.Views;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ergo.ListeRappels2018V1.Extensions
{
    public class AvailableTasksExt : PXCacheExtension<AvailableTasks>
    {
        #region UsrRappelsTotal
        [PXInt]
        [PXUIField(DisplayName = "Total")]
        public virtual int? UsrRappelsTotal { get; set; }
        public abstract class usrRappelsTotal : PX.Data.BQL.BqlInt.Field<usrRappelsTotal> { }
        #endregion

        #region UsrFiltresRappelsTotal
        [PXInt]
        [PXUIField(DisplayName = "Filtres")]

        public virtual int? UsrFiltresRappelsTotal { get; set; }
        public abstract class usrFiltresRappelsTotal : PX.Data.BQL.BqlInt.Field<usrFiltresRappelsTotal> { }
        #endregion
    }
}

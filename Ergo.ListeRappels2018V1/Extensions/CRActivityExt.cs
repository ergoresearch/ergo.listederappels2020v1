﻿using Ergo.ListeRappels2018V1.Enums.DAC;
using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTypes = Ergo.ListeRappels2018V1.Enums.LR201011.TaskTypes;

namespace Ergo.ListeRappels2018V1.Extensions
{
    public class CRActivityExt : PXCacheExtension<PX.Objects.CR.CRActivity>
    {
        #region PROPERTIES
        [PXDBString, PXUIField(DisplayName = "Succursale")]
        [PXSelector(typeof(Search<PX.Objects.GL.Branch.branchCD>))]
        public virtual String UsrBranchCD { get; set; }
        public abstract class usrBranchCD : IBqlField, IBqlOperand { }

        [PXDBInt, PXUIField(DisplayName = "Type")]
        [TaskTypes.List()]
        public virtual int? UsrSubType { get; set; }
        public abstract class usrSubType : IBqlField, IBqlOperand { }
        #endregion
    }
}

using System;

namespace ListeDeRappels.Contracts
{
    public interface IEnumColumn
    {
        string ColID { get; }
        string ColDescription { get; }
    }
}
﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Objects.GL;
using Ergo.ListeRappels2018V1.Enums.DAC;
using Ergo.ListeRappels2018V1.Enums.LR201011;
using RegionList = Ergo.ListeRappels2018V1.Enums.LR201011.RegionList;
using TaskTypes = Ergo.ListeRappels2018V1.Enums.LR201011.TaskTypes;

namespace Ergo.ListeRappels2018V1.DAC.Views
{
    [Serializable]
    [PXCacheName("AvailableTasks")]
    public class AvailableTasks : IBqlTable
    {
        #region BranchID
        [PXDBString()]
        [PXUIField(DisplayName = "Succursale")]
        [PXSelector(typeof(Search<Branch.branchCD>))]
        public virtual string BranchID { get; set; }
        public abstract class branchID : PX.Data.BQL.BqlInt.Field<branchID> { }
        #endregion

        #region Region
        [PXDBString(2, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Région")]
        [RegionList.List()]
        public virtual string Region { get; set; }
        public abstract class region : PX.Data.BQL.BqlString.Field<region> { }
        #endregion

        #region TaskType
        [PXDBInt()]
        [PXUIField(DisplayName = "Type")]
        [TaskTypes.List()]
        public virtual int? TaskType { get; set; }
        public abstract class taskType : PX.Data.BQL.BqlInt.Field<taskType> { }
        #endregion

        #region ForDate
        [PXDBDate()]
        [PXUIField(DisplayName = "Date")]
        public virtual DateTime? ForDate { get; set; }
        public abstract class forDate : PX.Data.BQL.BqlDateTime.Field<forDate> { }
        #endregion
    }
}

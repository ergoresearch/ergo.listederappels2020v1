﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ergo.ListeRappels2018V1.DAC.Views
{
    [Serializable]
    [PXCacheName("GridAvailableTasks")]
    public class GridAvailableTasks : IBqlTable
    {
        #region UIStatus
        [PXDBString(2, IsFixed = true, InputMask = "")]
        [PXUIField(DisplayName = "Status")]
        public virtual string UIStatus { get; set; }
        public abstract class uIStatus : PX.Data.BQL.BqlString.Field<uIStatus> { }
        #endregion

        #region UsrBranchCD
        [PXDBString(20, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Succursale")]
        public virtual string UsrBranchCD { get; set; }
        public abstract class usrBranchCD : PX.Data.BQL.BqlString.Field<usrBranchCD> { }
        #endregion

        #region EndDate
        [PXDBDate()]
        [PXUIField(DisplayName = "End Time")]
        public virtual DateTime? EndDate { get; set; }
        public abstract class endDate : PX.Data.BQL.BqlDateTime.Field<endDate> { }
        #endregion

        #region Subject
        [PXDBString(256, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Sommaire")]
        public virtual string Subject { get; set; }
        public abstract class subject : PX.Data.BQL.BqlString.Field<subject> { }
        #endregion

        #region Source
        [PXDBString()]
        [PXUIField(DisplayName = "Entité liée")]
        public virtual string Source { get; set; }
        public abstract class source : PX.Data.BQL.BqlString.Field<source> { }
        #endregion

        #region Phone1
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Téléphone 1 (Texto)")]
        public virtual string Phone1 { get; set; }
        public abstract class phone1 : PX.Data.BQL.BqlString.Field<phone1> { }
        #endregion

        #region Phone2
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Téléphone 2")]
        public virtual string Phone2 { get; set; }
        public abstract class phone2 : PX.Data.BQL.BqlString.Field<phone2> { }
        #endregion

        #region Phone3
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Téléphone 3")]
        public virtual string Phone3 { get; set; }
        public abstract class phone3 : PX.Data.BQL.BqlString.Field<phone3> { }
        #endregion

        #region OwnerID
        [PXDBString()]
        [PXUIField(DisplayName = "Responsable")]
        public virtual string OwnerID { get; set; }
        public abstract class ownerID : PX.Data.BQL.BqlGuid.Field<ownerID> { }
        #endregion

        #region Body
        [PXDBString(IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Détails de l'activité")]
        public virtual string Body { get; set; }
        public abstract class body : PX.Data.BQL.BqlString.Field<body> { }
        #endregion

        #region UsrSubType
        [PXDBInt()]
        [PXUIField(DisplayName = "Usr Sub Type")]
        public virtual int? UsrSubType { get; set; }
        public abstract class usrSubType : PX.Data.BQL.BqlInt.Field<usrSubType> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region Subcd
        [PXDBString(30, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Subcd")]
        public virtual string SubCD { get; set; }
        public abstract class subcd : PX.Data.BQL.BqlString.Field<subcd> { }
        #endregion

        #region Noteid
        [PXDBGuid(true, IsKey = true)]
        public virtual Guid? Noteid { get; set; }
        public abstract class noteid : PX.Data.BQL.BqlGuid.Field<noteid> { }
        #endregion

        #region RefNoteID
        [PXDBGuid()]
        public virtual Guid? RefNoteID { get; set; }
        public abstract class refnoteid : PX.Data.BQL.BqlGuid.Field<refnoteid> { }
        #endregion
    }
}

﻿using Ergo.ListeRappels2018V1.Constants;
using PX.Data;
using PX.Objects.CR;
using PX.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ergo.ListeRappels2018V1.DAC.Views
{
    [Serializable]
    [PXCacheName("CustomerActivities")]
    public class CustomerActivities : IBqlTable
    {
        #region StartDate
        [PXDBDate()]
        [PXUIField(DisplayName = "Date de début")]
        public virtual DateTime? StartDate { get; set; }
        public abstract class startDate : PX.Data.BQL.BqlDateTime.Field<startDate> { }
        #endregion

        #region EndDate
        [PXDBDate()]
        [PXUIField(DisplayName = "Heure de fin")]
        public virtual DateTime? EndDate { get; set; }
        public abstract class endDate : PX.Data.BQL.BqlDateTime.Field<endDate> { }
        #endregion

        #region Subject
        [PXDBString(256, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Sommaire")]
        public virtual string Subject { get; set; }
        public abstract class subject : PX.Data.BQL.BqlString.Field<subject> { }
        #endregion

        #region Body
        [PXDBString(IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Détails")]
        public virtual string Body { get; set; }
        public abstract class body : PX.Data.BQL.BqlString.Field<body> { }
        #endregion

        #region Noteid
        [PXDBGuid(true, IsKey = true)]
        [PXUIField(DisplayName = "Note ID")]
        [PXDBDefault(typeof(GridAvailableTasks.noteid))]
        [PXParent(typeof(Select<GridAvailableTasks,
                         Where<GridAvailableTasks.noteid,
                         Equal<Current<CustomerActivities.noteid>>>>))]
        public virtual Guid? Noteid { get; set; }
        public abstract class noteid : PX.Data.BQL.BqlGuid.Field<noteid> { }
        #endregion

        #region IsCompleteIcon
        [PXUIField(DisplayName = "Complete Icon", IsReadOnly = true)]
        [PXImage(HeaderImage = (Sprite.AliasControl + "@" + Sprite.Control.CompleteHead))]
        [PXFormula(typeof(Switch<Case<Where<GridAvailableTasks.uIStatus, Equal<ActivityStatusListAttribute.completed>>, ListeRappelsConstants.completed>>))]
        public virtual String IsCompleteIcon { get; set; }
        public abstract class isCompleteIcon : IBqlField { }
        #endregion

        #region RefNoteID
        [PXDBGuid()]
        public virtual Guid? RefNoteID { get; set; }
        public abstract class refnoteid : PX.Data.BQL.BqlGuid.Field<refnoteid> { }
        #endregion
    }
}

﻿using System;
using PX.Data;
using PX.Web.UI;

namespace Ergo.ListeRappels2018V1.Constants
{
    public class ListeRappelsConstants
    {
        public class completed : Constant<string>
        {
            public completed()
              : base(Sprite.Control.GetFullUrl(Sprite.Control.Complete))
            {
            }
        }

        public class taskOpenStatus : Constant<String>
        {
            public taskOpenStatus()
              : base("OP")
            {
            }
        }

        public class taskClosedStatus : Constant<String>
        {
            public taskClosedStatus()
              : base("CD")
            {
            }
        }

        public class taskCancelStatus : Constant<String>
        {
            public taskCancelStatus()
              : base("CL")
            {
            }
        }

        public class taskWeb : Constant<String>
        {
            public taskWeb()
              : base("WEBE    ")
            {
            }
        }

        public class taskInTreatmentStatus : Constant<String>
        {
            public taskInTreatmentStatus()
              : base("IT")
            {
            }
        }
        public class SubCDStartingIndex : Constant<int>
        {
            public SubCDStartingIndex()
              : base(10)
            {
            }
        }
        public class SubCDStartingLength : Constant<int>
        {
            public SubCDStartingLength()
              : base(2)
            {
            }
        }
    }
}

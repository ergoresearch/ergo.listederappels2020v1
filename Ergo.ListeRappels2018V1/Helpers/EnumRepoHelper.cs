using ListeDeRappels.Contracts;
using PX.Data;
using System;
using System.Linq;

namespace Ergo.ListeRappels2018V1.Helpers
{
    public static class EnumRepoHelper<T> where T : class, IBqlTable, IEnumColumn, new()
    {
        private static string[] _values;
        private static string[] _labels;

        static EnumRepoHelper()
        {
            PXResultset<T> res = PXSelect<T>.Select(new PXGraph());
            if (res is null) throw new ArgumentException("Type parameter is non existent");
            _values = res.Select(x => x.GetItem<T>().ColID).ToArray();
            _labels = res.Select(x => x.GetItem<T>().ColDescription).ToArray();
        }
        
        public static string[] Values() => _values;
        public static string[] Labels()  => _labels;
    }
}
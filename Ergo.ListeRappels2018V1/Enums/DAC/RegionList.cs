﻿using System;
using PX.Data;
using PX.Data.EP;
using ListeDeRappels.Contracts;

namespace Ergo.ListeRappels2018V1.Enums.DAC
{
    [Serializable]
    [PXCacheName("RegionList")]
    public class RegionList : IBqlTable, IEnumColumn
    {
        public abstract class iD : IBqlField { }
        public abstract class description : IBqlField { }


        [PXDBString(IsKey = true)]
        public virtual string ID { get; set; }

        [PXFieldDescription, PXDBString(50, IsUnicode = true, IsKey = false, InputMask = ""), PXDefault]
        public virtual string Description { get; set; }
        public string ColID { get => ID; }
        public string ColDescription { get => Description; }

    }
}

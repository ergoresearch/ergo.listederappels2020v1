﻿using ListeDeRappels.Contracts;
using PX.Data;
using PX.Data.EP;
using System;

namespace Ergo.ListeRappels2018V1.Enums.DAC
{
    [Serializable]
    [PXCacheName("TaskTypes")]
    public class TaskTypes : IBqlTable, IEnumColumn
    {
        public abstract class iD : IBqlField { }
        public abstract class description : IBqlField { }


        [PXDBString(IsKey = true)]
        public virtual string ID { get; set; }

        [PXFieldDescription, PXDBString(50, IsUnicode = true, IsKey = false, InputMask = ""), PXDefault]
        public virtual string Description { get; set; }
        public string ColID { get => ID; }
        public string ColDescription { get => Description; }
    }
}

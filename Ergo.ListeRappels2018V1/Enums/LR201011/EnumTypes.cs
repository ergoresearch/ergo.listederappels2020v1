using System;
using PX.Data;
using Ergo.ListeRappels2018V1.Helpers;

namespace Ergo.ListeRappels2018V1.Enums.LR201011
{
  public static class RegionList {

    public class ListAttribute : PXStringListAttribute {

            public static string[] values = EnumRepoHelper<DAC.RegionList>.Values();
            public static string[] labels = EnumRepoHelper<DAC.RegionList>.Labels();

            public ListAttribute()
        : base(values, labels) {
      }
    }
  }
          
  public static class TaskTypes {

    public class ListAttribute : PXIntListAttribute {

            public static int[] values = Array.ConvertAll(EnumRepoHelper<DAC.TaskTypes>.Values(), int.Parse);
            public static string[] labels = EnumRepoHelper<DAC.TaskTypes>.Labels();

            public ListAttribute()
        : base(values, labels) {
      }
    }
  }
          
          
}